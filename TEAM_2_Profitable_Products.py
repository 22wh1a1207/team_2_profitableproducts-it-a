import tkinter as tk
from tkinter import ttk

def slide2():
    root2.withdraw()

    def calculate():
        products_data = []
        for i in range(1, n + 1):
            product_name = entries_product_name[i].get()
            fixed_costs = float(entries_fixed_costs[i].get())
            selling_price = float(entries_selling_price[i].get())
            variable_costs = float(entries_variable_costs[i].get())

            breakeven_quantity = fixed_costs / (selling_price - variable_costs)
            total_cost = fixed_costs + (variable_costs * breakeven_quantity)
            total_revenue = selling_price * breakeven_quantity
            profit_or_loss = total_revenue - total_cost
            profit_or_loss_per_unit = profit_or_loss / breakeven_quantity
            variable_cost = variable_costs * breakeven_quantity
            variable_cost_per_unit = variable_costs

            products_data.append([
                product_name,
                f"{breakeven_quantity:.2f}",
                f"{total_cost:.2f}",
                f"{total_revenue:.2f}",
                f"{profit_or_loss:.2f}",
                f"{profit_or_loss_per_unit:.2f}",
                f"{variable_cost:.2f}",
                f"{variable_cost_per_unit:.2f}"
            ])

        # Update the table
        table.delete(*table.get_children())
        for product_data in products_data:
            table.insert("", "end", values=product_data)

    def calculate_all_breakeven():
        breakeven_quantities = []
        for i in range(1, n + 1):
            fixed_costs = float(entries_fixed_costs[i].get())
            selling_price = float(entries_selling_price[i].get())
            variable_costs = float(entries_variable_costs[i].get())

            breakeven_quantity = fixed_costs / (selling_price - variable_costs)
            breakeven_quantities.append(f"Product {i}: {breakeven_quantity:.2f}")

        result_label.config(text="\n".join(breakeven_quantities))

        closest_rows = []
        min_distance = float('inf')
        for i in range(25):
            quantity = breakeven_quantity + i * 0.01
            distance = abs(quantity - breakeven_quantity)
            if distance <= min_distance:
                min_distance = distance
                closest_rows.append(i)

    root = tk.Tk()
    root.title("Breakeven Calculator")

    n = int(input("Enter the number of products: "))

    table = ttk.Treeview(root, columns=("Product Name", "Breakeven Quantity", "Total Cost", "Total Revenue",
                                        "Profit or Loss", "Profit or Loss per Unit", "Variable Cost",
                                        "Variable Cost per Unit"), show="headings")

    table.heading("Product Name", text="Product Name")
    table.heading("Breakeven Quantity", text="Breakeven Quantity")
    table.heading("Total Cost", text="Total Cost")
    table.heading("Total Revenue", text="Total Revenue")
    table.heading("Profit or Loss", text="Profit or Loss")
    table.heading("Profit or Loss per Unit", text="Profit or Loss per Unit")
    table.heading("Variable Cost", text="Variable Cost")
    table.heading("Variable Cost per Unit", text="Variable Cost per Unit")

    entries_product_name = {}
    entries_fixed_costs = {}
    entries_selling_price = {}
    entries_variable_costs = {}

    for i in range(1, n + 1):
        label_product_name = tk.Label(root, text=f"Product {i} Name:")
        label_product_name.grid(row=i, column=0)

        entry_product_name = tk.Entry(root)
        entry_product_name.grid(row=i, column=1)
        entries_product_name[i] = entry_product_name

        entry_fixed_costs = tk.Entry(root)
        entry_fixed_costs.grid(row=i, column=2)
        entries_fixed_costs[i] = entry_fixed_costs

        entry_selling_price = tk.Entry(root)
        entry_selling_price.grid(row=i, column=3)
        entries_selling_price[i] = entry_selling_price

        entry_variable_costs = tk.Entry(root)
        entry_variable_costs.grid(row=i, column=4)
        entries_variable_costs[i] = entry_variable_costs

        entry_fixed_costs.config(bg="lightblue")
        entry_selling_price.config(bg="lightblue")
        entry_variable_costs.config(bg="lightblue")

    calculate_button = tk.Button(root, text="Calculate Single Product", command=calculate)
    calculate_all_button = tk.Button(root, text="Calculate All Breakeven", command=calculate_all_breakeven)
    result_label = tk.Label(root, text="", justify="left")
    background_color = "lightgray"

    root.configure(bg=background_color)
    table.grid(row=n + 1, column=0, columnspan=8)
    calculate_button.grid(row=n + 2, column=0, columnspan=4)
    calculate_all_button.grid(row=n + 2, column=4, columnspan=4)
    result_label.grid(row=n + 3, column=0, columnspan=8)


root2 = tk.Tk()
root2.geometry('1500x1500')
root2.config(bg='light pink')
button = tk.Button(root2, text='Next slide', font=("gabriola", 30), command=slide2).place(x=400, y=300)
Label = tk.Label(root2, text='PROFITABLE PRODUCTS', font=("gabriola", 30)).place(x=600, y=80)
img = tk.PhotoImage(file=r"C:\Users\Home\Desktop\wise pro\WhatsApp Image 2023-08-08 at 16.07.47.png").zoom(1, 1)

image_Label = tk.Label(root2, image=img).place(x=700, y=200)

root2.mainloop()
